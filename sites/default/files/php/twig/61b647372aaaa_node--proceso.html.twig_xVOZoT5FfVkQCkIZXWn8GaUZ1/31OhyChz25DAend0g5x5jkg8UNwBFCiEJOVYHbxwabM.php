<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/MiMuseo/templates/nodes/node--proceso.html.twig */
class __TwigTemplate_9d6848718b7fd5ce34714717af2476f28d8471e8dee134369246f9c946d60080 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo " ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->attachLibrary("bootstrap_barrio/node"), "html", null, true);
        echo " ";
        $context["classes"] = [0 => "node", 1 => ("node--type-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "bundle", [], "any", false, false, true, 13), 13, $this->source))), 2 => ((twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "isPromoted", [], "method", false, false, true, 13)) ? ("node--promoted") : ("")), 3 => ((twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "isSticky", [], "method", false, false, true, 13)) ? ("node--sticky") : ("")), 4 => (( !twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "isPublished", [], "method", false, false, true, 13)) ? ("node--unpublished") : ("")), 5 => ((        // line 14
($context["view_mode"] ?? null)) ? (("node--view-mode-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(($context["view_mode"] ?? null), 14, $this->source)))) : ("")), 6 => "clearfix"];
        // line 15
        echo "<div class=\"container-fluid\">
    <div class=\"container\">
        <div class=\"row justify-content-center\">
            <div class=\"col-12 col-sm-5 col-md-5 col-xl-5 col-lg-5 p-3\">
                <p>Alcance</p>
                ";
        // line 20
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "body", [], "any", false, false, true, 20), 20, $this->source), "html", null, true);
        echo "
            </div>
            <div class=\"col-12 col-sm-5 col-md-5 col-xl-5 col-lg-5 p-3\">
                ";
        // line 23
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_objetivo", [], "any", false, false, true, 23), 23, $this->source), "html", null, true);
        echo "
            </div>
        </div>
    </div>
</div>
<div class=\"container-fluid equipo-de-trabajo-procesos-misionales p-3\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-12 col-sm-12 col-md-12 col-xl-12 col-lg-12\">
                ";
        // line 32
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_equipo_de_trabajo", [], "any", false, false, true, 32), 32, $this->source), "html", null, true);
        echo "
            </div>
        </div>
    </div>
</div>
<div class=\"container-fluid\">
    <div class=\"container p-3\">
        <div class=\"row\">
            <p class=\"fuente-bold\">Documentación Relacionada</p>
            <div class=\"col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3\">
                <div class=\"accordion\" id=\"acordeon-proceso\">
                    <div class=\"accordion-item\">
                        <h2 class=\"accordion-header\" id=\"headingOne\">
                            <button class=\"accordion-button\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#collapseOne\" aria-expanded=\"true\" aria-controls=\"collapseOne\">
                                Caracterización
                            </button>
                        </h2>
                        <div id=\"collapseOne\" class=\"accordion-collapse collapse show\" aria-labelledby=\"headingOne\" data-bs-parent=\"#accordionExample\">
                            <div class=\"accordion-body\">
                                ";
        // line 51
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_documentos_caracterizacion", [], "any", false, false, true, 51), 51, $this->source), "html", null, true);
        echo "
                            </div>
                        </div>
                    </div>
                    <div class=\"accordion-item\">
                        <h2 class=\"accordion-header\" id=\"headingTwo\">
                            <button class=\"accordion-button collapsed\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#collapseTwo\" aria-expanded=\"false\" aria-controls=\"collapseTwo\">
                                Procedimientos
                            </button>
                        </h2>
                        <div id=\"collapseTwo\" class=\"accordion-collapse collapse\" aria-labelledby=\"headingTwo\" data-bs-parent=\"#accordionExample\">
                            <div class=\"accordion-body\">
                                ";
        // line 63
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_documentos_procedimient", [], "any", false, false, true, 63), 63, $this->source), "html", null, true);
        echo "
                            </div>
                        </div>
                    </div>
                    <div class=\"accordion-item\">
                        <h2 class=\"accordion-header\" id=\"headingThree\">
                            <button class=\"accordion-button collapsed\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#collapseThree\" aria-expanded=\"false\" aria-controls=\"collapseThree\">
                                Manuales
                            </button>
                        </h2>
                        <div id=\"collapseThree\" class=\"accordion-collapse collapse\" aria-labelledby=\"headingThree\" data-bs-parent=\"#accordionExample\">
                            <div class=\"accordion-body\">
                                ";
        // line 75
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_manuales", [], "any", false, false, true, 75), 75, $this->source), "html", null, true);
        echo "
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"col-12 col-sm-9 col-md-9 col-lg-9 col-xl-9\" style=\"border: 2px solid red;\">
                <h1>Visualizador de Documento</h1>
            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "themes/custom/MiMuseo/templates/nodes/node--proceso.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 75,  107 => 63,  92 => 51,  70 => 32,  58 => 23,  52 => 20,  45 => 15,  43 => 14,  39 => 13,);
    }

    public function getSourceContext()
    {
        return new Source("", "themes/custom/MiMuseo/templates/nodes/node--proceso.html.twig", "C:\\wamp64\\www\\mi-museo\\themes\\custom\\MiMuseo\\templates\\nodes\\node--proceso.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 13);
        static $filters = array("escape" => 13, "clean_class" => 13);
        static $functions = array("attach_library" => 13);

        try {
            $this->sandbox->checkSecurity(
                ['set'],
                ['escape', 'clean_class'],
                ['attach_library']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
